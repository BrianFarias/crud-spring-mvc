
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <title>Mantenedor Bennu</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Agregar Nuevo Profesor</h4>
                </div>
                <div class="card-body">
                    <form method="POST">  
                        
                        <label>Nombre Profesor</label>
                        <input type="text" name="nomprof" class="form-control">
                        <br>                        
                        <label>Fecha Nacimiento</label>
                        <input type="text" name="fecprof" class="form-control">
                        <br>                        
                        <label>Estado</label>
                        <input type="text" name="estado" class="form-control">
                        <br>                        
                        <label>Id Colegio</label>
                        <input type="text" name="idcolegio" class="form-control">
                        
                        <label>Id Asignatura</label>
                        <input type="text" name="idasign" class="form-control">
                        
                        <input type="submit" name="Agregarp" class="btn btn-success">
                    
                        <a href="indexp.htm" class="btn btn-warning">Regresar</a>
                        
                    </form>
                </div>
            </div>             
        </div>
    </body>
</html>
