
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>Mantenedor Bennu</title>
    </head>
    <body>
        <div class="container mt-4">
            <div class="card border-info">
                <div class="card-header bg-info text-white">


                </div>                                    
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>                            
                            <th>ID PROFESOR</th>
                            <th>NOMBRE</th>
                            <th>FECHA</th>
                            <th>ESTADO</th>
                            <th>ID COLEGIO</th>
                            <th>ID ASIGNATURA</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>                      

                        <c:forEach var="datop" items="${listap}">
                            <tr>
                                <td>${datop.id_profesor}</td>
                                <td>${datop.nombre} </td>
                                <td>${datop.fecnac} </td>   
                                <td>${datop.estado} </td> 
                                <td>${datop.id_colegio} </td> 
                                <td>${datop.id_asignatura} </td> 
                                <td>
                                    <a href="editarp.htm?idp=${datop.id_profesor}" class="btn btn-warning"> Editar </a>
                                    <a href="deletep.htm?idp=${datop.id_profesor}" class="btn btn-danger"> Delete</a>
                                    <a href="index.htm?" class="btn btn-danger">Regresar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="card-header bg-info text-white">
                <a href="agregarp.htm" class="btn btn-primary" >Nuevo Registro</a> 
            </div>  
        </div>

    </body>


</html>
