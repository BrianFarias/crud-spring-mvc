
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <title>Mantenedor Bennu</title>
    </head>
    <body>
        
  
        <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Editar Colegio</h4>
                </div>
                <div class="card-body">
                    <form method="POST">                 
                        <label>Nombre Colegio</label>
                        <input type="text" name="nom" class="form-control" value="${lista[0].nombre}">
                        <br>
                        <label>Direccion</label>
                        <input type="text" name="direccion" class="form-control"value="${lista[0].direccion}">
                        <br>

                        <input type="submit" name="Actualizar" class="btn btn-success">
                    
                        <a href="index.htm" class="btn btn-warning">Regresar</a>
                        
                    </form>
                </div>
            </div>             
        </div>
    </body>
</html>
