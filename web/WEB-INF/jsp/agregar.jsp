
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <title>Mantenedor Bennu</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Agregar Nuevo Colegio</h4>
                </div>
                <div class="card-body">
                    <form method="POST">                   
                        <label>Nombre Colegio</label>
                        <input type="text" name="nom" class="form-control">
                        <br>                        
                        <label>Direccion</label>
                        <input type="text" name="direccion" class="form-control">
                        <br>
                        
                        <input type="submit" name="Agregar" class="btn btn-success">
                    
                        <a href="index.htm" class="btn btn-warning">Regresar</a>
                        
                    </form>
                </div>
            </div>             
        </div>
    </body>
</html>
