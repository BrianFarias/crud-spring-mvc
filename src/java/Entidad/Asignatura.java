
package Entidad;

public class Asignatura {
    int idasig;
    String nomasig;

    public Asignatura() {
    }

    public Asignatura(int idasig, String nomasig) {
        this.idasig = idasig;
        this.nomasig = nomasig;
    }

    public int getIdasig() {
        return idasig;
    }

    public void setIdasig(int idasig) {
        this.idasig = idasig;
    }

    public String getNomasig() {
        return nomasig;
    }

    public void setNomasig(String nomasig) {
        this.nomasig = nomasig;
    }
    
}
