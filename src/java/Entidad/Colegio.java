
package Entidad;

public class Colegio {
  int id;
  String nom;
  String direccion;

    public Colegio() {
    }

    public Colegio(int id, String nom, String direccion) {
        this.id = id;
        this.nom = nom;
        this.direccion = direccion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
 
    
}
