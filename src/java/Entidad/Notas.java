
package Entidad;

 
public class Notas {
     
    int idnota;
    int idasig;
    int idalumno;
    double nota;

    public Notas() {
    }

    public Notas(int idnota, int idasig, int idalumno, double nota) {
        this.idnota = idnota;
        this.idasig = idasig;
        this.idalumno = idalumno;
        this.nota = nota;
    }

    public int getIdnota() {
        return idnota;
    }

    public void setIdnota(int idnota) {
        this.idnota = idnota;
    }

    public int getIdasig() {
        return idasig;
    }

    public void setIdasig(int idasig) {
        this.idasig = idasig;
    }

    public int getIdalumno() {
        return idalumno;
    }

    public void setIdalumno(int idalumno) {
        this.idalumno = idalumno;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }
    
}
