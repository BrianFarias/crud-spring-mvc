
package Entidad;

import java.util.Date;


public class Alumno {
    int idalumno;
    String nomalumno;
    Date fecalumno;

    public Alumno() {
    }

    public Alumno(int idalumno, String nomalumno, Date fecalumno) {
        this.idalumno = idalumno;
        this.nomalumno = nomalumno;
        this.fecalumno = fecalumno;
    }

    public int getIdalumno() {
        return idalumno;
    }

    public void setIdalumno(int idalumno) {
        this.idalumno = idalumno;
    }

    public String getNomalumno() {
        return nomalumno;
    }

    public void setNomalumno(String nomalumno) {
        this.nomalumno = nomalumno;
    }

    public Date getFecalumno() {
        return fecalumno;
    }

    public void setFecalumno(Date fecalumno) {
        this.fecalumno = fecalumno;
    }
    
}
