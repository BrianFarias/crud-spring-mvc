
package Entidad;

import java.util.Date;



public class Profesor {
    
    int idp;
    String nomprof;
    String fecprof;
    String estado;
    int idcolegio;
    int idasign;

    public Profesor() {
    }

    public Profesor(int idp, String nomprof, String fecprof, String estado, int idcolegio, int idasign) {
        this.idp = idp;
        this.nomprof = nomprof;
        this.fecprof = fecprof;
        this.estado = estado;
        this.idcolegio = idcolegio;
        this.idasign = idasign;
    }

    public int getIdp() {
        return idp;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public String getNomprof() {
        return nomprof;
    }

    public void setNomprof(String nomprof) {
        this.nomprof = nomprof;
    }

    public String getFecprof() {
        return fecprof;
    }

    public void setFecprof(String fecprof) {
        this.fecprof = fecprof;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIdcolegio() {
        return idcolegio;
    }

    public void setIdcolegio(int idcolegio) {
        this.idcolegio = idcolegio;
    }

    public int getIdasign() {
        return idasign;
    }

    public void setIdasign(int idasign) {
        this.idasign = idasign;
    }
    
}