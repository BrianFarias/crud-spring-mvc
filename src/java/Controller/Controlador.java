package Controller;

import Config.Conexion;
import Entidad.Colegio;
import Entidad.Profesor;
import static java.lang.Integer.parseInt;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HttpServletBean;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Controlador {

    Conexion con = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(con.Conectar());
    ModelAndView mav = new ModelAndView();
    int id;
    List datos;

    @RequestMapping("index.htm")
    public ModelAndView Listar() {
        String sql = "select * from colegio";
        datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", datos);
        mav.setViewName("index");
        return mav;

    }

    @RequestMapping(value = "agregar.htm", method = RequestMethod.GET)
    public ModelAndView Agregar() {
        mav.addObject(new Colegio());
        mav.setViewName("agregar");
        return mav;
    }

    @RequestMapping(value = "agregar.htm", method = RequestMethod.POST)
    public ModelAndView Agregar(Colegio p) {
        String sql = "insert into colegio (id_colegio,nombre,direccion) values (?,?,?)";
        this.jdbcTemplate.update(sql, p.getId(), p.getNom(), p.getDireccion());
        return new ModelAndView("redirect:/index.htm");
    }

    @RequestMapping(value = "editar.htm", method = RequestMethod.GET)
    public ModelAndView Editar(HttpServletRequest request) {

        id = Integer.parseInt(request.getParameter("id"));
        String sql = "select * from colegio where id_colegio=" + id;
        datos = this.jdbcTemplate.queryForList(sql);
        mav.setViewName("editar");
        mav.addObject("lista", datos);
        return mav;
    }

    @RequestMapping(value = "editar.htm", method = RequestMethod.POST)
    public ModelAndView Editar(Colegio p) {

        String sql = "update colegio set nombre=?,direccion=? where id_colegio=" + id;
        this.jdbcTemplate.update(sql, p.getNom(), p.getDireccion());
        return new ModelAndView("redirect:/index.htm");
    }

    @RequestMapping(value = "delete.htm", method = RequestMethod.GET)
    public ModelAndView Delete(HttpServletRequest request) {

        id = Integer.parseInt(request.getParameter("id"));
        String sql = "delete from colegio where id_colegio=" + id;
        this.jdbcTemplate.update(sql);
        return new ModelAndView("redirect:/index.htm");
    }
   
}
