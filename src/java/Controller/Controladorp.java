/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Profesor;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

public class Controladorp {
     Conexion conp = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(conp.Conectar());
    ModelAndView mav = new ModelAndView();
    int idp;
    List datosp;
    
    @RequestMapping("indexp.htm")
    public ModelAndView Listarp() {
        String sql = "select * from profesor";
        datosp = this.jdbcTemplate.queryForList(sql);
        mav.addObject("listap", datosp);
        mav.setViewName("indexp");
        return mav;

    }

    @RequestMapping(value = "agregarp.htm", method = RequestMethod.GET)
    public ModelAndView Agregarp() {      
        mav.addObject(new Profesor());
        mav.setViewName("agregarp");
        return mav;
    }

    @RequestMapping(value = "agregarp.htm", method = RequestMethod.POST)
    public ModelAndView Agregarp(Profesor p) {
        String sql = "insert into profesor (id_profesor,nombre,fecnac,estado,id_colegio,id_asignatura) values (?,?,?,?,?,?)";
        this.jdbcTemplate.update(sql,p.getIdp(), p.getNomprof(), p.getFecprof(), p.getEstado(),p.getIdcolegio(),p.getIdasign());
        return new ModelAndView("redirect:/indexp.htm");
    }

    @RequestMapping(value = "editarp.htm", method = RequestMethod.GET)
    public ModelAndView Editarp(HttpServletRequest request) {

        idp = Integer.parseInt(request.getParameter("idp"));
        String sql = "select * from profesor where id_profesor=" + idp;
        datosp = this.jdbcTemplate.queryForList(sql);
        mav.setViewName("editarp");
        mav.addObject("listap", datosp);
        return mav;
    }

    @RequestMapping(value = "editarp.htm", method = RequestMethod.POST)
    public ModelAndView Editarp(Profesor p) {

        String sql = "update profesor set nombre=?,fecnac=?,estado=?,id_colegio=?,id_asignatura=? where id_profesor=" + idp;
        this.jdbcTemplate.update(sql, p.getNomprof(), p.getFecprof(), p.getEstado(),p.getIdcolegio(),p.getIdasign());
        return new ModelAndView("redirect:/indexp.htm");
    }

    @RequestMapping(value = "deletep.htm", method = RequestMethod.GET)
    public ModelAndView Deletep(HttpServletRequest request) {

        idp = Integer.parseInt(request.getParameter("idp"));
        String sql = "delete from profesor where id_profesor=" + idp;
        this.jdbcTemplate.update(sql);
        return new ModelAndView("redirect:/indexp.htm");
    }
   
    
}
